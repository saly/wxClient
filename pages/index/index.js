Page({
    data: {
        channelList: [],//频道列表
        currentID: '',//当前频道ID
        newList: [],//新闻列表
        scrollHeight: 0,//页面高度
        scrollTop: 0,//当前Y轴滚动条位置
        currentPage: 1,//当前页数
        touchXStart: 0,//滑动X轴开始位置
        touchYStart: 0,//滑动Y轴开始位置
    },
    onLoad: function () {
        this.loadChannelList();
    },
    initData: function () {//初始化数据
        this.setData({
            scrollTop: 0,
            newList: [],
            currentPage: 1,
            scrollHeight: 0
        });
    },
    refreshNewList: function (e) {//获取最新数据
        this.initData();
        this.loadNewlList(this.data.currentID);
    },
    loadMoreNew: function (e) {//加载更多
        this.setData({currentPage: this.data.currentPage + 1});
        this.loadNewlList(this.data.currentID);
    },
    setCurrentYScroll: function (event) {//设置当前Y轴滚动条位置
        this.setData({
            scrollTop: event.detail.scrollTop
        });
    },
    loadChannelList: function () {//加载频道列表
        let that = this;
        wx.request({
            url: 'http://route.showapi.com/109-34',
            data: {
                showapi_sign: '3c3c44ade4c440c2a84814f5969fe32d',
                showapi_appid: '30465'
            },
            success: function (res) {
                let temp_data = res.data.showapi_res_body.channelList;
                let data = temp_data.slice(0, 14);//只获取14个频道
                data.forEach(function (item, index) {
                    if (item.name.length == 4) {//国内焦点
                        item.name = item.name.substring(0, 2);
                    }
                    else if (item.name.length == 5) {//互联网焦点
                        item.name = item.name.substring(0, 3);
                    }
                });
                that.setData({channelList: data, currentID: data[0].channelId});
                that.loadNewlList(data[0].channelId);
            }
        })
    },
    loadNewlList: function (channelId = "") {//加载新闻列表
        let that = this;
        that.showToast();
        wx.request({
            url: 'http://route.showapi.com/109-35',
            data: {
                channelId: channelId,
                showapi_sign: '3c3c44ade4c440c2a84814f5969fe32d',
                showapi_appid: '30465',
                page: that.data.currentPage,
                needAllList: 0,
                needContent: 1
            },
            success: function (res) {
                let temp_data = res.data.showapi_res_body.pagebean.contentlist;
                if (that.data.newList.length != 0) {
                    that.setData({newList: that.data.newList.concat(temp_data)});
                }
                else {
                    that.setData({newList: temp_data});
                }
                wx.getSystemInfo({//设置scroll view高度
                    success: function (res) {
                        that.setData({
                            scrollHeight: res.windowHeight,
                        });
                    }
                });
                that.hideToast();
            }
        })
    },
    handerTap: function (e) {//频道点击事件
        let that = this;
        let tempList = this.data.channelList;
        tempList.forEach(function (item, index) {
            if (e.target.id == item.channelId) {
                that.initData();
                that.setData({currentID: item.channelId});
                that.loadNewlList(item.channelId);
            }
        });

    },
    handerNavigator: function (e) {//处理跳转
        let index = e.currentTarget.dataset.index;
        let data = {
            'content': this.data.newList[index].content,
            title: this.data.newList[index].title,
            source: this.data.newList[index].source,
            pubDate: this.data.newList[index].pubDate,
        };
        wx.setStorageSync("new_detail", data);
        wx.navigateTo({url: '../detail/detail'});
    },
    handerTouchStart: function (e) {//滑动X,Y开始位置
        let temp = [];
        this.setData({touchXStart: e.changedTouches[0].clientX, touchYStart: e.changedTouches[0].clientY});
    },
    handerTouchEnd: function (e) {//判断向左还是向右滑动
        let distanceX = this.data.touchXStart - e.changedTouches[0].clientX;
        let distanceY = this.data.touchYStart - e.changedTouches[0].clientY;
        let that = this;
        let tempList = this.data.channelList;
        let tempIndex = 0;
        tempList.forEach(function (item, index) {
            if (that.data.currentID == item.channelId) {
                tempIndex = index;
            }
        });
        if (Math.abs(distanceX) > Math.abs(distanceY) && distanceX < 0) {//向左
            if (tempIndex != 0) {//如果不是第一个则可以向左滑动
                that.initData();
                that.setData({currentID: tempList[tempIndex - 1].channelId});
                that.loadNewlList(tempList[tempIndex].channelId);
            }
        }
        else if (Math.abs(distanceX) > Math.abs(distanceY) && distanceX > 0) {//向右
            if (tempIndex != tempList.length) {//如果不是最后一个则可以向右滑动
                that.initData();
                that.setData({currentID: tempList[tempIndex + 1].channelId});
                that.loadNewlList(tempList[tempIndex].channelId);
            }
        }
    },
    showToast: function () {
        wx.showToast({
            title: '加载中',
            icon: 'loading',
            mask:true,
            duration: 10000
        });
    },
    hideToast:function () {
        wx.hideToast();
    }
});