Page({
    data: {
        windowHeight: 0
    },
    onLoad: function () {
        let that = this;
        wx.getSystemInfo({//设置高度
            success: function (res) {
                that.setData({windowHeight: res.windowHeight});
            }
        });
    },
    onReady: function (e) {
        // 使用 wx.createMapContext 获取 map 上下文
        this.mapCtx = wx.createMapContext('myMap');
        this.mapCtx.getCenterLocation({
            success: function (res) {
                console.log(res.longitude)
                console.log(res.latitude)
            }
        });

    }
})