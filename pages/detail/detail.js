// pages/detail/detail.js
Page({
    data: {
        new: {}
    },
    onLoad: function (options) {
        this.setData({new: wx.getStorageSync('new_detail')});
    },
    onReady: function () {
        // 页面渲染完成
    },
    onShow: function () {
        // 页面显示
    },
    onHide: function () {
        // 页面隐藏
    },
    onUnload: function () {
        // 页面关闭
    },
    onShareAppMessage: function () {
        return {
            title: '我的头条',
            path: '/detail/detail'
        }
    }
});